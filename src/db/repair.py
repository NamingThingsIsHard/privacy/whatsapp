"""

"""
import os
import sqlite3

__author__ = "LoveIsGrief"


def attempt_repair(db_path):
    print("trying to repair android database...")
    repairedfile = db_path + "_repaired.db"
    if os.path.exists(repairedfile):
        os.remove(repairedfile)
    os.system('echo .dump | sqlite3 "%s" > Temp.sql' % db_path)
    os.system('echo .quit | sqlite3 -init Temp.sql "%s"' % repairedfile)
    if os.path.exists("Temp.sql"):
        os.remove("Temp.sql")

    # Check if the repair was successful
    msgstore = sqlite3.connect(repairedfile)
    msgstore.execute("SELECT * FROM chat_list")
