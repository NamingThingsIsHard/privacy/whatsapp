"""

"""
import base64
import datetime

from constants import CONTENT_GPS, CONTENT_VCARD, CONTENT_VIDEO, CONTENT_IMAGE, CONTENT_MEDIA_NOTHUMB, CONTENT_TEXT
from db.models import Message, ChatSession

__author__ = "LoveIsGrief"


class IphoneChatSession(ChatSession):
    def _init_date(self, lastmessagedate):
        lastmessagedate = str(lastmessagedate)
        if lastmessagedate.find(
                ".") > -1:  # if timestamp is not like "304966548", but like "306350664.792749", then just use the numbers in front of the "."
            lastmessagedate = lastmessagedate[:lastmessagedate.find(".")]
        self.last_message_date = datetime.datetime.fromtimestamp(int(lastmessagedate) + 11323 * 60 * 1440)


class IphoneMessage(Message):
    def _init_date(self, msgdate):
        msgdate = str(msgdate)
        if msgdate.find(
                ".") > -1:  # if timestamp is not like "304966548", but like "306350664.792749", then just use the numbers in front of the "."
            msgdate = msgdate[:msgdate.find(".")]
        self.msg_date = datetime.datetime.fromtimestamp(int(msgdate) + 11323 * 60 * 1440)

    def _init_content_type(self):
        content_type = CONTENT_TEXT
        self.media_wa_type = "0"

        # GPS
        if self.latitude and self.longitude:
            content_type = CONTENT_GPS
            self.media_wa_type = "5"
            gpsname = None
        # VCARD
        elif self.vcard_string:
            content_type = CONTENT_VCARD
            self.media_wa_type = "4"
        # AUDIO?
        # MEDIA
        elif self.media_url:
            if self.media_thumb:
                if self.media_wa_type == "3":
                    content_type = CONTENT_VIDEO
                else:
                    content_type = CONTENT_IMAGE
                    self.media_wa_type = "1"
                # content_type = CONTENT_MEDIA_THUMB
            else:
                content_type = CONTENT_MEDIA_NOTHUMB

        self.content_type = content_type


