"""

"""
from db.iphone.models import IphoneChatSession
from db.models import Message
from db.sub_readers import MessageReader, ChatSessionReader

__author__ = "LoveIsGrief"


class IphoneMessageReader(MessageReader):

    def _query_messages(self, chat_session):
        return self.msgstore_connection.execute("SELECT * FROM ZWAMESSAGE WHERE ZCHATSESSION=? ORDER BY Z_PK ASC;",
                                                [chat_session.pk_cs])

    def build_from_row(self, msg_row):
        #  IPHONE ChatStorage.sqlite file *** ZWACHATSESSION TABLE  #
        # ------------------------------------------------------- #
        # ------------------------------------------------------- #
        # msg_row[0] --> Z_PK (primary key)
        # msg_row[1] --> Z_ENT
        # msg_row[2] --> Z_OPT
        # msg_row[3] --> ZISFROMME
        # msg_row[4] --> ZSORT
        # msg_row[5] --> ZMESSAGESTATUS
        # msg_row[6] --> ZMESSAGETYPE
        # msg_row[7] --> ZMEDIAITEM
        # msg_row[8] --> ZCHATSESSION
        # msg_row[9] --> ZMESSAGEDATE
        # msg_row[10] -> ZTEXT
        # msg_row[11] -> ZTOJID
        # msg_row[12] -> ZFROMJID
        # msg_row[13] -> ZSTANZAID
        if msg_row["ZISFROMME"] == 1:
            contactfrom = "me"
        else:
            contactfrom = msg_row["ZFROMJID"]
        if msg_row["ZMEDIAITEM"] is None:
            curr_message = Message(msg_row["Z_PK"], msg_row["ZISFROMME"], None, msg_row["ZMESSAGEDATE"],
                                   msg_row["ZTEXT"], contactfrom, msg_row["ZMESSAGESTATUS"], None, None,
                                   None,
                                   None, None, None, None, None, None, None)
        else:
            c2 = self.msgstore_connection.execute("SELECT * FROM ZWAMEDIAITEM WHERE Z_PK=?;", [msg_row["ZMEDIAITEM"]])
            media = c2.fetchone()
            # ------------------------------------------------------- #
            #  IPHONE ChatStorage.sqlite file *** ZWAMEDIAITEM TABLE  #
            # ------------------------------------------------------- #
            # Z_PK INTEGER PRIMARY KEY
            # Z_ENT INTEGER
            # Z_OPT INTEGER
            # ZMOVIEDURATION INTEGER
            # ZMEDIASAVED INTEGER
            # ZFILESIZE INTEGER
            # ZMESSAGE INTEGER
            # ZLONGITUDE FLOAT
            # ZHACCURACY FLOAT
            # ZLATITUDE FLOAT
            # ZVCARDSTRING VARCHAR
            # ZXMPPTHUMBPATH VARCHAR
            # ZMEDIALOCALPATH VARCHAR
            # ZMEDIAURL VARCHAR
            # ZVCARDNAME VARCHAR
            # ZTHUMBNAILLOCALPATH VARCHAR
            # ZTHUMBNAILDATA BLOB
            try:
                movieduration = media["ZMOVIEDURATION"]
            except:
                movieduration = 0
            if movieduration > 0:
                mediawatype = "3"
            else:
                mediawatype = msg_row["ZMESSAGETYPE"]
            try:
                ZXMPPTHUMBPATH = media["ZXMPPTHUMBPATH"]
            except:
                ZXMPPTHUMBPATH = None
            curr_message = Message(msg_row["Z_PK"], msg_row["ZISFROMME"], None, msg_row["ZMESSAGEDATE"],
                                   msg_row["ZTEXT"], contactfrom, msg_row["ZMESSAGESTATUS"],
                                   media["ZMEDIALOCALPATH"], media["ZMEDIAURL"],
                                   media["ZTHUMBNAILDATA"], ZXMPPTHUMBPATH, mediawatype,
                                   media["ZFILESIZE"], media["ZLATITUDE"], media["ZLONGITUDE"],
                                   media["ZVCARDNAME"], media["ZVCARDSTRING"])
        return curr_message


class IphoneChatSessionReader(ChatSessionReader):

    def get_all(self):
        chat_session_list = []
        chat_rows_query = self.msgstore_connection.execute("SELECT * FROM ZWACHATSESSION")
        for chat_row in chat_rows_query:
            # ---------------------------------------------------------- #
            #  IPHONE  ChatStorage.sqlite file *** ZWACHATSESSION TABLE  #
            # ---------------------------------------------------------- #
            # chat_row[0] --> Z_PK (primary key)
            # chat_row[1] --> Z_ENT
            # chat_row[2] --> Z_OPT
            # chat_row[3] --> ZINCLUDEUSERNAME
            # chat_row[4] --> ZUNREADCOUNT
            # chat_row[5] --> ZCONTACTABID
            # chat_row[6] --> ZMESSAGECOUNTER
            # chat_row[7] --> ZLASTMESSAGEDATE
            # chat_row[8] --> ZCONTACTJID
            # chat_row[9] --> ZSAVEDINPUT
            # chat_row[10] -> ZPARTNERNAME
            # chat_row[11] -> ZLASTMESSAGETEXT

            try:
                status_query = self.msgstore_connection.execute(
                    "SELECT ZSTATUSTEXT FROM ZWASTATUS WHERE ZCONTACTABID =?;", [chat_row["ZCONTACTABID"]])
                statustext = status_query.fetchone()[0]
            except:
                statustext = None
            # ---------------------------------------------------------- #
            #  IPHONE  ChatStorage.sqlite file *** ZWASTATUS TABLE       #
            # ---------------------------------------------------------- #
            # Z_PK (primary key)
            # Z_ENT
            # Z_OPT
            # ZEXPIRATIONTIME
            # ZCONTACTABID
            # ZNOPUSH
            # ZFAVORITE
            # ZSTATUSDATE
            # ZPHONENUMBER
            # status_query.fetchone()[0] --> ZSTATUSTEXT
            # ZWHATSAPPID
            curr_chat = IphoneChatSession(chat_row["Z_PK"], chat_row["ZPARTNERNAME"], chat_row["ZCONTACTJID"],
                                          chat_row["ZMESSAGECOUNTER"], chat_row["ZUNREADCOUNT"], statustext,
                                          chat_row["ZLASTMESSAGEDATE"])
            chat_session_list.append(curr_chat)

        return chat_session_list
