"""

"""
import base64

__author__ = "LoveIsGrief"

UNAVAILABLE = "N/A"


class ChatSession(object):

    # init
    def __init__(self, pkcs, contactname, contactid,
                 contactmsgcount, contactunread, contactstatus, lastmessagedate):

        # if invalid params are passed, sets attributes to invalid values
        # primary key
        if pkcs == "" or pkcs is None:
            self.pk_cs = -1
        else:
            self.pk_cs = pkcs

        # contact id
        if contactid == "" or contactid is None:
            self.contact_id = UNAVAILABLE
        else:
            self.contact_id = contactid

        # contact nick
        if contactname == "" or contactname is None:
            if self.contact_id != UNAVAILABLE:
                self.contact_name = self.contact_id.split('@')[0]
            else:
                self.contact_name = self.contact_id
        else:
            self.contact_name = contactname

        # contact unread msg
        if contactunread == "" or contactunread is None:
            self.contact_unread_msg = UNAVAILABLE
        else:
            self.contact_unread_msg = contactunread

        # contact status
        if contactstatus == "" or contactstatus is None:
            self.contact_status = UNAVAILABLE
        else:
            self.contact_status = contactstatus

        # contact last message date
        if lastmessagedate == "" or lastmessagedate is None or lastmessagedate == 0:
            self.last_message_date = " N/A"  # space is necessary for that the empty chats are placed at the end on sorting
        else:
            self._init_date(lastmessagedate)

        # chat session messages
        self.messages = []

    def _init_date(self, lastmessagedate):
        raise NotImplementedError()

    def init_messages(self, message_reader):
        self.messages = message_reader.get(self)

    @property
    def message_count(self):
        return len(self.messages)

    # comparison operator
    def __cmp__(self, other):
        if self.pk_cs == other.pk_cs:
            return 0
        return 1


class Message:

    # init
    def __init__(self, pkmsg, fromme, keyid, msgdate, text, contactfrom, msgstatus,
                 localurl, mediaurl, mediathumb, mediathumblocalurl, mediawatype, mediasize, latitude, longitude,
                 vcardname, vcardstring):

        # if invalid params are passed, sets attributes to invalid values
        # primary key
        if pkmsg == "" or pkmsg is None:
            self.pk_msg = -1
        else:
            self.pk_msg = pkmsg

        # msg key id
        if keyid == "" or keyid is None:
            self.keyid = None
        else:
            self.key_id = keyid

        # "from me" flag
        if fromme == "" or fromme is None:
            self.from_me = False
        else:
            self.from_me = fromme

        # message timestamp
        if msgdate == "" or msgdate is None or msgdate == 0:
            self.msg_date = UNAVAILABLE
        else:
            self._init_date(msgdate)

        # message text
        if text == "" or text is None:
            self.msg_text = UNAVAILABLE
        else:
            self.msg_text = text
        # contact from
        if contactfrom == "" or contactfrom is None:
            self.contact_from = UNAVAILABLE
        else:
            self.contact_from = contactfrom

        # media
        if localurl == "" or localurl is None:
            self.local_url = ""
        else:
            self.local_url = localurl
        if mediaurl == "" or mediaurl is None:
            self.media_url = ""
        else:
            self.media_url = mediaurl
        if mediathumb == "" or mediathumb is None:
            self.media_thumb = ""
        else:
            self.media_thumb = mediathumb
        if mediathumblocalurl == "" or mediathumblocalurl is None:
            self.media_thumb_local_url = ""
        else:
            self.media_thumb_local_url = mediathumblocalurl
        if mediawatype == "" or mediawatype is None:
            self.media_wa_type = ""
        else:
            self.media_wa_type = mediawatype
        if mediasize == "" or mediasize is None:
            self.media_size = ""
        else:
            self.media_size = mediasize

        # status
        if msgstatus == "" or msgstatus is None:
            self.status = UNAVAILABLE
        else:
            self.status = msgstatus

        # GPS
        if latitude == "" or latitude is None:
            self.latitude = ""
        else:
            self.latitude = latitude
        if longitude == "" or longitude is None:
            self.longitude = ""
        else:
            self.longitude = longitude

        # VCARD
        if vcardname == "" or vcardname is None:
            self.vcard_name = ""
        else:
            self.vcard_name = vcardname
        if vcardstring == "" or vcardstring is None:
            self.vcard_string = ""
        else:
            self.vcard_string = vcardstring

        self._init_media_thumb()
        self._init_content_type()
        self._init_rest()

    def _init_content_type(self):
        raise NotImplementedError()

    def _init_date(self, msgdate):
        # TODO: Generalize this
        raise NotImplementedError()

    def _init_media_thumb(self):
        # prepare thumb
        if self.media_thumb:
            self.media_thumb = "data:image/jpg;base64,\n" + base64.b64encode(self.media_thumb).decode("utf-8")
        elif self.media_thumb_local_url:
            self.media_thumb = self.media_thumb_local_url

    def _init_rest(self):
        pass
        #         < !--  date
        #         elaboration
        #         for further use -->
        #         date = str(message.msg_date)[:10]
        #         if date != 'N/A' and date != 'N/A error':
        #             date = int(date.replace("-", ""))
        #
        #     < !--  Display
        #     Msg
        #     content(Text / Media) -->
        #
        #     try:
        #         if content_type == CONTENT_IMAGE:
        #
        # < !--  Search
        # for offline file with current date (+3 days) and known media size -->
        # linkimage = findfile("IMG", message.media_size, message.local_url, date, 3)
        # try:
        #     '<td class="text"><a onclick="image(this.href);return(false);" target="image" href="{}"><img src="{}" alt="Image"/></a>&nbsp;|&nbsp;<a onclick="image(this.href);return(false);" target="image" href="{}">Image</a>'.format(
        #         linkimage, message.media_thumb, message.media_url)
        # except:
        #     < td
        #
        #
        #     class ="text" > Image N / A'
        # elif content_type == CONTENT_AUDIO:
        # < !--  Search
        # for offline file with current date (+3 days) and known media size -->
        # linkaudio = findfile("AUD", message.media_size, message.local_url, date, 3)
        # try:
        #     '<td class="text"><a onclick="media(this.href);return(false);" target="media" href="{}">Audio (offline)</a>&nbsp;|&nbsp;<a onclick="media(this.href);return(false);" target="media" href="{}">Audio (online)</a>'.format(
        #         linkaudio, message.media_url)
        # except:
        #     < td
        #
        #
        #     class ="text" > Audio N / A'
        # elif content_type == CONTENT_VIDEO:
        # < !--  Search
        # for offline file with current date (+3 days) and known media size -->
        # linkvideo = findfile("VID", message.media_size, message.local_url, date, 3)
        # try:
        #     '<td class="text"><a onclick="media(this.href);return(false);" target="media" href="{}"><img src="{}" alt="Video"/></a>&nbsp;|&nbsp;<a onclick="media(this.href);return(false);" target="media" href="{}">Video</a>'.format(
        #         linkvideo, message.media_thumb, message.media_url)
        # except:
        #     < td
        #
        #
        #     class ="text" > Video N / A'
        # elif content_type == CONTENT_MEDIA_THUMB:
        # < !--  Search
        # for offline file with current date (+3 days) and known media size -->
        # linkmedia = findfile("MEDIA_THUMB", message.media_size, message.local_url, date, 3)
        # try:
        #     '<td class="text"><a onclick="image(this.href);return(false);" target="image" href="{}"><img src="{}" alt="Media"/></a>&nbsp;|&nbsp;<a onclick="image(this.href);return(false);" target="image" href="{}">Media</a>'.format(
        #         linkmedia, message.media_thumb, message.media_url)
        # except:
        #     < td
        #
        #
        #     class ="text" > Media N / A'
        # elif content_type == CONTENT_MEDIA_NOTHUMB:
        # < !--  Search
        # for offline file with current date (+3 days) and known media size -->
        # linkmedia = findfile("MEDIA_NOTHUMB", message.media_size, message.local_url, date, 3)
        # try:
        #     '<td class="text"><a onclick="media(this.href);return(false);" target="media" href="{}">Media (online)</a>&nbsp;|&nbsp;<a onclick="media(this.href);return(false);" target="media" href="{}">Media (online)</a>'.format(
        #         linkmedia, message.media_url)
        # except:
        #     < td
        #
        #
        #     class ="text" > Media N / A'
        # elif content_type == CONTENT_VCARD:
        # if message.vcard_name == "" or message.vcard_name is None:
        #     vcardintro = ""
        # else:
        #     vcardintro = "CONTACT: <b>" + message.vcard_name + "</b><br>\n"
        # message.vcard_string = message.vcard_string.replace("\n", "<br>\n")
        # try:
        #     < td
        #
        #
        #     class ="text" > {}'.format(vcardintro + message.vcard_string)
        # except:
        #     < td
        #
        #
        #     class ="text" > VCARD N / A'
        # elif content_type == CONTENT_GPS:
        # try:
        #     if gpsname == "" or gpsname == None:
        #         gpsname = ""
        #     else:
        #         gpsname = "\n" + gpsname
        #     gpsname = gpsname.replace("\n", "<br>\n")
        #     if message.media_thumb:
        #         '<td class="text"><a onclick="image(this.href);return(false);" target="image" href="https://maps.google.com/?q={},{}"><img src="{}" alt="GPS"/></a>{}'.format(
        #             message.latitude, message.longitude, message.media_thumb, gpsname)
        #     else:
        #         '<td class="text"><a onclick="image(this.href);return(false);" target="image" href="https://maps.google.com/?q={},{}">GPS: {}, {}</a>{}'.format(
        #             message.latitude, message.longitude, message.latitude, message.longitude, gpsname)
        # except:
        #     < td
        #
        #
        #     class ="text" > GPS N / A'
        # elif content_type == CONTENT_NEWGROUPNAME:
        # content_type = CONTENT_OTHER
        # elif content_type != CONTENT_TEXT:
        # content_type = CONTENT_OTHER
        # < !--  End
        # of
        # If - Clause, now
        # text or other
        # type
        # of
        # content: -->
        # if content_type == CONTENT_TEXT or content_type == CONTENT_OTHER:
        #     msgtext = convert_smileys(message.msg_text)
        #     msgtext = re.sub(r'(http[^\s\n\r]+)',
        #                      r'<a onclick="image(this.href);return(false);" target="image" href="\1">\1</a>',
        #                      msgtext)
        #     msgtext = re.sub(r'((?<!\S)www\.[^\s\n\r]+)',
        #                      r'<a onclick="image(this.href);return(false);" target="image" href="http://\1">\1</a>',
        #                      msgtext)
        #     msgtext = msgtext.replace("\n", "<br>\n")
        #     try:
        #         < td
        #
        #
        #         class ="text" > {}'.format(msgtext)
        #     except:
        #         < td
        #
        #
        #         class ="text" > N / A'
        # except:
        # print("error in message id " + message.pk_msg + "(key_id: " + message.key_id + ")")
        # < td
        #
        #
        # class ="text" > N / A (error in message) < / td >

    # comparison operator
    def __cmp__(self, other):
        if self.pk_msg == other.pk_msg:
            return 0
        return 1
