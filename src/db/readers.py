"""

"""
import logging
import sqlite3

from constants import IPHONE, ANDROID
from db.android.readers import AndroidChatSessionReader, AndroidMessageReader
from db.iphone.readers import IphoneMessageReader, IphoneChatSessionReader
from db.sub_readers import ChatSessionReader, MessageReader

__author__ = "LoveIsGrief"


class MessageStoreReader(object):

    def __init__(self, message_store_path, wa_file_path=None):
        self.message_store_path = message_store_path
        self.wa_file_path = wa_file_path
        self.msgstore_connection = None
        self.have_wa = False
        self.chat_session_list = []
        self.cursor = None
        self._chat_session_reader = None  # type: ChatSessionReader
        self._message_reader = None  # type: MessageReader
        self._mode = None
        self._owner = None

    def __enter__(self):
        self.msgstore_connection = sqlite3.connect(self.message_store_path)
        self.msgstore_connection.row_factory = sqlite3.Row
        self._check_db_connection()
        self._init_sub_readers()
        return self

    def _check_db_connection(self):
        try:
            self.msgstore_connection.execute("select 1")
        except sqlite3.DatabaseError:
            logging.warning("Couldn't open database '%s'.", self.message_store_path)
            raise

    def _init_sub_readers(self):
        if self.mode == ANDROID:
            self._chat_session_reader = AndroidChatSessionReader(self.msgstore_connection, self.wa_file_path)
            self._message_reader = AndroidMessageReader(self.msgstore_connection)
        else:
            self._chat_session_reader = IphoneChatSessionReader(self.msgstore_connection, self.wa_file_path)
            self._message_reader = IphoneMessageReader(self.msgstore_connection)

    def __exit__(self, exc_type, exc_val, exc_tb):
        # TODO: Add properties for each of these fields to make access to uninitialized fields throw an Exception
        self.msgstore_connection.close()
        self.msgstore_connection = None
        self._chat_session_reader = None
        self._message_reader = None

    def get_chat_sessions_with_messages(self):
        chat_sessions = self._chat_session_reader.get_all()
        for chat_session in chat_sessions:
            chat_session.init_messages(self._message_reader)
        return chat_sessions

    @property
    def mode(self):
        if not self._mode:
            # check on platform
            try:
                self.msgstore_connection.execute("SELECT * FROM ZWACHATSESSION LIMIT 1")
                # if succeeded --> IPHONE mode
                self._mode = IPHONE
                print("iPhone mode!\n")
            except:
                # if failed --> ANDROID mode
                self._mode = ANDROID
                print("Android mode!\n")
        return self._mode

    @property
    def owner(self):
        if self._owner is None:
            if self.mode == ANDROID:
                # c1.execute("SELECT key_remote_jid FROM messages WHERE key_from_me=1 AND key_remote_jid LIKE '%@s.whatsapp.net%' ")
                # doesn't work
                self._owner = ""
            elif self.mode == IPHONE:
                c1 = self.msgstore_connection.execute("SELECT ZFROMJID FROM ZWAMESSAGE WHERE ZISFROMME=1")
                try:
                    self._owner = (c1.fetchone()[0]).split('/')[0]
                except:
                    self._owner = ""

        return self._owner


class WhatsAppContactsReader(object):

    def __init__(self, wa_path):
        self.wa_path = wa_path


def validate_sqlite_file(file_path):
    with open(file_path, 'rb') as sqlite_file:
        return sqlite_file.read(6).decode('ascii').lower() != 'sqlite'
