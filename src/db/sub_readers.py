"""

"""
import sqlite3

from .models import ChatSession

__author__ = "LoveIsGrief"


class MessageReader(object):

    def __init__(self, store_connection):
        self.msgstore_connection = store_connection

    def get(self, chat_session):
        # for each chat session, gets all messages
        msg_list = []
        cursor = self._query_messages(chat_session)
        message_count = 1
        for msg_row in cursor:
            try:
                msg_list.append(self.build_from_row(msg_row))
                message_count += 1
            except sqlite3.Error as msg:
                print(
                    'Error while reading message #{} in chat #{}: {}'.format(message_count, chat_session.pk_cs, msg))
            except TypeError as msg:
                print(
                    'Error while reading message #{} in chat #{}: {}'.format(message_count, chat_session.pk_cs, msg))
        return msg_list

    def build_from_row(self, row):
        raise NotImplementedError()

    def _query_messages(self, chat_session):
        raise NotImplementedError()


class ChatSessionReader(object):

    def __init__(self, msgstore_connection, wa_connection):
        self.msgstore_connection = msgstore_connection
        self.wa_connection = wa_connection
        self.thumbs_cache = {}

    def get_all(self):
        """

        :return:
        :rtype: list[ChatSession]
        """
        raise NotImplementedError()
