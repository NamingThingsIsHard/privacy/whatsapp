"""

"""
import base64
import datetime

from constants import CONTENT_VIDEO, CONTENT_IMAGE, CONTENT_AUDIO, CONTENT_TEXT, CONTENT_GPS, CONTENT_NEWGROUPNAME, \
    CONTENT_VCARD
from db.models import ChatSession, Message

__author__ = "LoveIsGrief"


class AndroidChatSession(ChatSession):

    def _init_date(self, lastmessagedate):
        msgdate = str(lastmessagedate)
        # cut last 3 digits (microseconds)
        msgdate = msgdate[:-3]
        self.last_message_date = datetime.datetime.fromtimestamp(int(msgdate))


class AndroidMessage(Message):

    def _init_date(self, msgdate):
        msgdate = str(msgdate)
        # cut last 3 digits (microseconds)
        msgdate = msgdate[:-3]
        self.msg_date = datetime.datetime.fromtimestamp(int(msgdate))

    def _init_content_type(self):
        content_type = None
        if self.from_me == 1:
            if self.status == 6:
                content_type = CONTENT_NEWGROUPNAME
        if content_type is None:
            if self.media_wa_type == "4":
                content_type = CONTENT_VCARD
                self.vcard_string = self.msg_text
                self.vcard_name = self.local_url
            elif self.media_wa_type == "1" or self.media_wa_type == "3" or self.media_wa_type == "5":
                # TODO: move this to the _init_media_thumb method
                if str(self.msg_text)[:3] == "/9j":
                    self.media_thumb = "data:image/jpg;base64,\n" + self.msg_text
                else:
                    try:
                        self.media_thumb = "data:image/jpg;base64,\n" + base64.b64encode(self.media_thumb).decode(
                            "utf-8")
                    except:
                        self.media_thumb = ""
                if self.media_wa_type == "5":
                    content_type = CONTENT_GPS
                    if self.local_url:
                        gpsname = self.local_url
                    else:
                        gpsname = None
                else:
                    if self.media_wa_type == "3":
                        content_type = CONTENT_VIDEO
                    else:
                        content_type = CONTENT_IMAGE
            else:
                if self.media_wa_type == "2":
                    content_type = CONTENT_AUDIO
                else:
                    content_type = CONTENT_TEXT
        self.content_type = content_type
