"""

"""

from db.android.models import AndroidChatSession, AndroidMessage
from db.sub_readers import ChatSessionReader, MessageReader

__author__ = "LoveIsGrief"


class AndroidChatSessionReader(ChatSessionReader):

    def get_all(self):
        chat_session_list = []
        if self.wa_connection:
            wa = self.wa_connection.execute("SELECT * FROM wa_contacts WHERE is_whatsapp_user = 1 GROUP BY jid")
            for chats in wa:
                # ------------------------------------------ #
                #  ANDROID WA.db file *** wa_contacts TABLE  #
                # ------------------------------------------ #
                # chats[0] --> _id (primary key)
                # chats[1] --> jid
                # chats[2] --> is_whatsapp_user
                # chats[3] --> is_iphone
                # chats[4] --> status
                # chats[5] --> number
                # chats[6] --> raw_contact_id
                # chats[7] --> display_name
                # chats[8] --> phone_type
                # chats[9] --> phone_label
                # chats[10] -> unseen_msg_count
                # chats[11] -> photo_ts
                try:
                    c2 = self.msgstore_connection.execute(
                        "SELECT message_table_id FROM chat_list WHERE key_remote_jid=?", [chats["jid"]])
                    lastmessage = c2.fetchone()[0]
                    # !todo alternativ maximale _id WHERE key_remote_jid
                    c2.execute("SELECT timestamp FROM messages WHERE _id=?", [lastmessage])
                    lastmessagedate = c2.fetchone()[0]
                except:  # not all contacts that are whatsapp users may already have been chatted with
                    lastmessagedate = None
                curr_chat = AndroidChatSession(chats["_id"], chats["display_name"], chats["jid"], None,
                                               chats["unseen_msg_count"], chats["status"], lastmessagedate)
                chat_session_list.append(curr_chat)
        else:
            c1 = self.msgstore_connection.execute("SELECT * FROM chat_list")
            for chats in c1:
                # ---------------------------------------------- #
                #  ANDROID MSGSTORE.db file *** chat_list TABLE  #
                # ---------------------------------------------- #
                # chats[0] --> _id (primary key)
                # chats[1] --> key_remote_jid (contact jid or group chat jid)
                # chats[2] --> message_table_id (id of last message in this chat, corresponds to table messages primary key)
                name = chats["key_remote_jid"].split('@')[0]
                try:
                    lastmessage = chats["message_table_id"]
                    c2 = self.msgstore_connection.execute("SELECT timestamp FROM messages WHERE _id=?", [lastmessage])
                    lastmessagedate = c2.fetchone()[0]
                except:
                    lastmessagedate = None
                curr_chat = AndroidChatSession(chats["_id"], name, chats["key_remote_jid"], None, None, None,
                                               lastmessagedate)
                chat_session_list.append(curr_chat)
                # now retrieve thumbnail database
        c2 = self.msgstore_connection.execute("SELECT key_id, thumbnail FROM message_thumbnails")
        for row in c2:
            self.thumbs_cache[row["key_id"]] = row["thumbnail"]
        return chat_session_list


class AndroidMessageReader(MessageReader):
    def _query_messages(self, chat_session):
        return self.msgstore_connection.execute("SELECT * FROM messages WHERE key_remote_jid=? ORDER BY _id ASC;",
                                                [chat_session.contact_id])

    def build_from_row(self, msg_row, thumbs_cache=None):
        # --------------------------------------------- #
        #  ANDROID MSGSTORE.db file *** messages TABLE  #
        # --------------------------------------------- #
        # msg_row[0] --> _id (primary key)
        # msg_row[1] --> key_remote_jid
        # msg_row[2] --> key_from_me
        # msg_row[3] --> key_id
        # msg_row[4] --> status
        # msg_row[5] --> needs_push
        # msg_row[6] --> data
        # msg_row[7] --> timestamp
        # msg_row[8] --> media_url
        # msg_row[9] --> media_mime_type
        # msg_row[10] -> media_wa_type
        # msg_row[11] -> media_size
        # msg_row[12] -> media_name
        # msg_row[13] -> latitude
        # msg_row[14] -> longitude
        # msg_row[15] -> thumb_image
        # msg_row[16] -> remote_resource
        # msg_row[17] -> received_timestamp
        # msg_row[18] -> send_timestamp
        # msg_row[19] -> receipt_server_timestamp
        # msg_row[20] -> receipt_device_timestamp
        # message sender
        thumbs_cache = thumbs_cache or {}
        if msg_row["remote_resource"] == "" or msg_row["remote_resource"] is None:
            contactfrom = msg_row["key_remote_jid"]
        else:
            contactfrom = msg_row["remote_resource"]
        if msg_row["key_from_me"] == 1:
            contactfrom = "me"
        thumbnaildata = None
        if msg_row["raw_data"] is not None:
            try:
                thumbnaildata = msg_row["raw_data"]
            except:
                thumbnaildata = None
        elif msg_row["thumb_image"] is not None:
            thumbnaildata = thumbs_cache.get(msg_row["key_id"])
        return AndroidMessage(msg_row["_id"], msg_row["key_from_me"], msg_row["key_id"], msg_row["timestamp"],
                              msg_row["data"], contactfrom, msg_row["status"], msg_row["media_name"],
                              msg_row["media_url"], thumbnaildata, None, msg_row["media_wa_type"],
                              msg_row["media_size"], msg_row["latitude"], msg_row["longitude"], None, None)
