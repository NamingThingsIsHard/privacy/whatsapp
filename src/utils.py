"""

"""
import os
import re
from os import path as ospath

__author__ = "LoveIsGrief"

# build the list of emojis from the ones we can actually replace
EMOJI_DIR = ospath.normpath(ospath.join(ospath.dirname(__file__), "..", "data", "emoji"))
# convert the filename to a unicode character
EMOJIS = [chr(int(ospath.splitext(emoji_png)[0], 16)) for emoji_png in os.listdir(EMOJI_DIR)]


def convert_smileys(text):
    """
    Replace smileys by an html image

    :param text:
    :type text: basestring
    :return:
    :rtype:
    """
    regex = "(%s)" % "|".join(EMOJIS)

    def replacer(match):
        unicode = match.group(0)
        hex_string = r'{0:x}'.format(ord(unicode[0])).upper()
        return """<img src="/data/emoji/%s.png" alt="%s"/>""" % (hex_string, unicode)

    return re.sub(regex, replacer, text)
