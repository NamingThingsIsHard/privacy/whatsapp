"""

"""
import os

__author__ = "LoveIsGrief"
IPHONE = 1
ANDROID = 2
CONTENT_TEXT = 0
CONTENT_IMAGE = 1
CONTENT_AUDIO = 2
CONTENT_VIDEO = 3
CONTENT_VCARD = 4
CONTENT_GPS = 5
CONTENT_NEWGROUPNAME = 6
CONTENT_MEDIA_THUMB = 7
CONTENT_MEDIA_NOTHUMB = 8
CONTENT_OTHER = 99

MAIN_DIR = os.path.normpath(
    os.path.join(
        os.path.abspath(os.path.dirname(__file__)),
        "..",
    )
)

DATA_PATH = os.path.join(MAIN_DIR, "data")
LIB_PATH = os.path.join(MAIN_DIR, "lib")
