# -*- coding: utf-8 -*-
'''
WhatsApp Xtract v2.2
- WhatsApp Backup Messages Extractor for Android and iPhone

Released on November 17th, 2012
Last Update on November 17th, 2012 (v2.2)

Tested with Whatsapp (Android) 2.8.5732
Tested with Whatsapp (iPhone)  2.5.1

Changelog:

v2.2 (updated by Martina Weidner - Nov 17, 2012)
- now supports new emoji smileys
- (Android Version) hotfix for TypeError in b64encode
- (Android Version) decoded file won't be deleted even if it can't be opened

v2.1 (updated by Fabio Sangiacomo and Martina Weidner - May 7, 2012)
- improved install pyCrypto.bat
- added easy drag and drop possibility with whatsapp_xtract_drag'n'drop_database(s)_here.bat
- (Android Version) added support to fix corrupted android whatsapp database (needs sqlite3, for windows sqlite3.exe is contained in the archive)
- (Android Version) removed wrong extraction of owner in android version
- (Iphone Version) information from Z_METADATA table will be printed to shell
- (Iphone Version) fixed bug in support of older Iphone whatsapp databases

V2.0 (updated by Fabio Sangiacomo and Martina Weidner - Apr 26, 2012)
- supports WhatsApp DBs coming from both Android and iPhone platforms
- (Android Version) wa.db is optional
- (Android Version) now also crypted msgstore.db from the SD card can be imported
- chat list is sorted by the last sent message
- fixed bug that the script didn't work with python 3

V1.3 (updated by Martina Weidner - Apr 17, 2012)
- corrected linking of offline files (now linking according to media file size)

V1.2 (updated by Martina Weidner - Apr 5, 2012)
- media files also linked to offline files
- corrected hyperlinks

V1.1 (updated by Martina Weidner - Apr 5, 2012)
- changed database structure, Android only
- show contact names
- show smileys
- show images
- link / popup for images, video, audio, gps
- clickable links

V1.0 (created by Fabio Sangiacomo - Dec 10, 2011)
- first release, iPhone only:
  it takes in input the file "ChatStorage.sqlite",
  extracts chat sessions and the bare text 
- sortable js allows table sorting to make chat sessions easily readable

Usage:

For Android DB:
> python whatsapp_xtract.py msgstore.db -w wa.db
OR (if wa.db is unavailable)
> python whatsapp_xtract.py msgstore.db
OR (for crypted db)
> python whatsapp_xtract.py msgstore.db.crypt

For iPhone DB: (-w option is ignored)
> python whatsapp_xtract.py ChatStorage.sqlite

(C)opyright 2012 Fabio Sangiacomo <fabio.sangiacomo@digital-forensics.it>
(C)opyright 2012 Martina Weidner  <martina.weidner@freenet.ch>

Released under MIT licence

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

'''

import datetime
import glob
import os
import sys
import time
from argparse import ArgumentParser

from jinja2 import Environment, FileSystemLoader

from constants import IPHONE, ANDROID, DATA_PATH, LIB_PATH
from db.readers import MessageStoreReader
from distutils.dir_util import copy_tree

THIS_PATH = os.path.normpath(os.path.abspath(os.path.dirname(__file__)))


################################################################################
# Functions for Find Offline File
def filelistonce(folder, date):
    flistnames = glob.glob(os.path.join(folder, '*' + date + '*'))
    flistsizes = []
    for i in range(len(flistnames)):
        statinfo = os.stat(flistnames[i])
        fsize = statinfo.st_size
        flistsizes.append(fsize)
    flist = [flistnames, flistsizes]
    return flist


def filelist(type, date):
    folder = None
    flist = None
    if type == 'IMG':
        folder = 'Media/WhatsApp Images/'
        if not date in flistimg:
            flistimg[date] = filelistonce(folder, date)
        flist = flistimg[date]
    elif type == 'AUD':
        folder = 'Media/WhatsApp Audio/'
        if not date in flistaud:
            flistaud[date] = filelistonce(folder, date)
        flist = flistaud[date]
    elif type == 'VID':
        folder = 'Media/WhatsApp Video/'
        if not date in flistvid:
            flistvid[date] = filelistonce(folder, date)
        flist = flistvid[date]
    return folder, flist


def findfile(type, size, localurl, date, additionaldays):
    fname = ''
    try:
        if mode == ANDROID:
            date = str(date)
            z = 0
            while fname == '':
                z = z + 1
                # use or create list of all media files of that type and that date
                folder, flist = filelist(type, date)
                # search the file with the given size
                try:
                    fname = flist[0][flist[1].index(size)]
                except:
                    fname = ''
                if fname == '':
                    if z >= 1 + additionaldays:
                        # if file is not found for "today", then try for "tomorrow" and "the day after tomorrow". If it's still not found, then try to find the temporary file, if not successful then just link the media folder
                        if os.path.exists(folder + localurl):
                            fname = folder + localurl
                        else:
                            fname = folder
                    else:
                        timestamptoday = int(
                            str(time.mktime(datetime.datetime.strptime(date, "%Y%m%d").timetuple()))[:-2])
                        timestamptomorrow = timestamptoday + 86400
                        tomorrow = str(datetime.datetime.fromtimestamp(timestamptomorrow))[:10]
                        tomorrow = tomorrow.replace("-", "")
                        date = tomorrow
        elif mode == IPHONE:
            # folder = 'Media/'
            # if os.path.exists(localurl):
            #    fname = localurl
            # else:
            #    fname = folder
            fname = localurl
    except:
        print("error while searching for files in " + localurl + " at date " + date)
    # return the file name
    return fname


################################################################################
################################################################################
# MAIN
def main(argv):
    # parser options
    parser = ArgumentParser(description='Converts a Whatsapp database to HTML.')
    parser.add_argument(dest='infile',
                        help="input 'msgstore.db' or 'msgstore.db.crypt' (Android) or 'ChatStorage.sqlite' (iPhone) file to scan")
    parser.add_argument('-w', '--wafile', dest='wafile',
                        help="optionally input 'wa.db' (Android) file to scan")
    parser.add_argument('-o', '--output-dir',
                        help="optionally choose name of output directory")
    options = parser.parse_args()

    # checks for the wadb file
    if options.wafile is None:
        have_wa = False
    elif not os.path.exists(options.wafile):
        print('"{}" file is not found!'.format(options.wafile))
        sys.exit(1)
    else:
        have_wa = True

    # checks for the input file
    if options.infile is None:
        parser.print_help()
        sys.exit(1)
    msgstore_path = os.path.abspath(options.infile)
    if not os.path.exists(msgstore_path):
        print('"{}" file is not found!'.format(msgstore_path))
        sys.exit(1)

    loader = FileSystemLoader(os.path.join(THIS_PATH, "templates"))
    env = Environment(
        loader=loader
    )
    main_template = env.get_template("main.html.jinja")
    session_template = env.get_template("session.html.jinja")

    with MessageStoreReader(msgstore_path, options.wafile) as store_reader:
        owner = store_reader.owner
        mode = store_reader.mode
        chat_session_list = store_reader.get_chat_sessions_with_messages()

    # OUTPUT
    output_dir = options.output_dir or owner
    if not output_dir:
        output_dir = options.infile.replace(".crypt", '').replace(".db", '')

    output_dir = os.path.normpath(os.path.abspath(output_dir))

    os.makedirs(output_dir, 0o770, True)

    # Copy all required data and libs
    copy_tree(os.path.join(DATA_PATH), output_dir)
    copy_tree(os.path.join(LIB_PATH), os.path.join(output_dir, "lib"))

    # The index file
    main_html_path = os.path.join(output_dir, "main.html")
    with open(main_html_path, 'w') as main_html_file:
        print("printing output to " + main_html_path + " ...")
        main_html_file.write(main_template.render(
            ANDROID=ANDROID,
            IPHONE=IPHONE,
            chat_session_list=chat_session_list,
            mode=mode,
            page_type="main"
        ))

    # Write all chat sessions
    for chat_session in chat_session_list:
        session_html_path = os.path.join(output_dir, "%s_%s.html" % (
            chat_session.contact_name, chat_session.pk_cs
        ))
        with open(session_html_path, 'w') as session_html_file:
            print("printing session to " + session_html_path + " ...")
            session_html_file.write(session_template.render(
                contact_id=chat_session.contact_id,
                contact_name=chat_session.contact_name,
                messages=chat_session.messages,
                page_type="session",
                pk_cs=chat_session.pk_cs,
            ))


if __name__ == '__main__':
    main(sys.argv[1:])
